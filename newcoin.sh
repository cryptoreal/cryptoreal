#!/bin/bash

COINNAME=cryptoreal
COINCODE=CRL
SED=sed
GSED=gsed
FIND=find
GFIND=/usr/local/opt/findutils/libexec/gnubin/find
GREP=grep
GGREP=ggrep

export SED

rename() {
  NEWNAME=$(echo "${@:3}" | $GSED -r -e 's/'$1'/'$2'/g')
  mv ${@:3} $NEWNAME
}

export -f rename

# rename files
find . -type f -name "*bitcoin*" -exec bash -c 'rename bitcoin cryptoreal "{}"' \;
find . -type d -name "*bitcoin*" -exec bash -c 'rename bitcoin cryptoreal "{}"' \;

# rename content in files
for a in $(ggrep -Ril bitcoin .|grep -v ./.git|grep -v newcoin.sh); do
  $GSED -i -e 's/bitcoind/cryptoreald/g' $a
  $GSED -i -e 's/bitcoin/cryptoreal/g' $a
  $GSED -i -e 's/Bitcoin/Cryptoreal/g' $a
  $GSED -i -e 's/BitCoin/CryptoReal/g' $a
  $GSED -i -e 's/BITCOIN/CRYPTOREAL/g' $a
done
for a in $(ggrep -Ril BTC .|grep -v ./.git|grep -v newcoin.sh); do
  $GSED -i -e 's/BTC/CRL/g' $a
  $GSED -i -e 's/btc/crl/g' $a
done

# ports
$GSED -i -e 's/18333/18555/g' src/chainparams.cpp
$GSED -i -e 's/8333/8555/g' src/chainparams.cpp
$GSED -i -e 's/8332/8554/g' src/chainparamsbase.cpp
$GSED -i -e 's/18443/18665/g' src/chainparamsbase.cpp


# messages codes to replace in src/chainparams.cpp
# pchMessageStart[0] = 0xf9;
# pchMessageStart[1] = 0xbe;
# pchMessageStart[2] = 0xb4;
# pchMessageStart[3] = 0xd9;
# to
# pchMessageStart[0] = 0xe8;
# pchMessageStart[1] = 0xad;
# pchMessageStart[2] = 0xa3;
# pchMessageStart[3] = 0xc8;

# pchMessageStart[0] = 0x0b;
# pchMessageStart[1] = 0x11;
# pchMessageStart[2] = 0x09;
# pchMessageStart[3] = 0x07;
# to
# pchMessageStart[0] = 0x1c;
# pchMessageStart[1] = 0x22;
# pchMessageStart[2] = 0x1a;
# pchMessageStart[3] = 0x18;

# pchMessageStart[0] = 0xfa;
# pchMessageStart[1] = 0xbf;
# pchMessageStart[2] = 0xb5;
# pchMessageStart[3] = 0xda;
# to
# pchMessageStart[0] = 0xfb;
# pchMessageStart[1] = 0xcf;
# pchMessageStart[2] = 0xc6;
# pchMessageStart[3] = 0xeb;

# DB4 compile
# ./contrib/install_db4.sh .
# export BDB_PREFIX='/Users/anderson/Dev/crypto/cryptoreal/db4'
# ./configure BDB_LIBS="-L${BDB_PREFIX}/lib -ldb_cxx-4.8" BDB_CFLAGS="-I${BDB_PREFIX}/include" 
