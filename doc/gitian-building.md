Gitian building
================

This file was moved to [the Cryptoreal Core documentation repository](https://github.com/cryptoreal-core/docs/blob/master/gitian-building.md) at [https://github.com/cryptoreal-core/docs](https://github.com/cryptoreal-core/docs).
