Cryptoreal Core
=============

Setup
---------------------
Cryptoreal Core is the original Cryptoreal client and it builds the backbone of the network. It downloads and, by default, stores the entire history of Cryptoreal transactions, which requires a few hundred gigabytes of disk space. Depending on the speed of your computer and network connection, the synchronization process can take anywhere from a few hours to a day or more.

To download Cryptoreal Core, visit [cryptorealcore.org](https://cryptorealcore.org/en/download/).

Running
---------------------
The following are some helpful notes on how to run Cryptoreal Core on your native platform.

### Unix

Unpack the files into a directory and run:

- `bin/cryptoreal-qt` (GUI) or
- `bin/cryptoreald` (headless)

### Windows

Unpack the files into a directory, and then run cryptoreal-qt.exe.

### macOS

Drag Cryptoreal Core to your applications folder, and then run Cryptoreal Core.

### Need Help?

* See the documentation at the [Cryptoreal Wiki](https://en.cryptoreal.it/wiki/Main_Page)
for help and more information.
* Ask for help on [#cryptoreal](https://webchat.freenode.net/#cryptoreal) on Freenode. If you don't have an IRC client, use [webchat here](https://webchat.freenode.net/#cryptoreal).
* Ask for help on the [CryptorealTalk](https://cryptorealtalk.org/) forums, in the [Technical Support board](https://cryptorealtalk.org/index.php?board=4.0).

Building
---------------------
The following are developer notes on how to build Cryptoreal Core on your native platform. They are not complete guides, but include notes on the necessary libraries, compile flags, etc.

- [Dependencies](dependencies.md)
- [macOS Build Notes](build-osx.md)
- [Unix Build Notes](build-unix.md)
- [Windows Build Notes](build-windows.md)
- [FreeBSD Build Notes](build-freebsd.md)
- [OpenBSD Build Notes](build-openbsd.md)
- [NetBSD Build Notes](build-netbsd.md)
- [Gitian Building Guide (External Link)](https://github.com/cryptoreal-core/docs/blob/master/gitian-building.md)

Development
---------------------
The Cryptoreal repo's [root README](/README.md) contains relevant information on the development process and automated testing.

- [Developer Notes](developer-notes.md)
- [Productivity Notes](productivity.md)
- [Release Notes](release-notes.md)
- [Release Process](release-process.md)
- [Source Code Documentation (External Link)](https://doxygen.cryptorealcore.org/)
- [Translation Process](translation_process.md)
- [Translation Strings Policy](translation_strings_policy.md)
- [JSON-RPC Interface](JSON-RPC-interface.md)
- [Unauthenticated REST Interface](REST-interface.md)
- [Shared Libraries](shared-libraries.md)
- [BIPS](bips.md)
- [Dnsseed Policy](dnsseed-policy.md)
- [Benchmarking](benchmarking.md)

### Resources
* Discuss on the [CryptorealTalk](https://cryptorealtalk.org/) forums, in the [Development & Technical Discussion board](https://cryptorealtalk.org/index.php?board=6.0).
* Discuss project-specific development on #cryptoreal-core-dev on Freenode. If you don't have an IRC client, use [webchat here](https://webchat.freenode.net/#cryptoreal-core-dev).
* Discuss general Cryptoreal development on #cryptoreal-dev on Freenode. If you don't have an IRC client, use [webchat here](https://webchat.freenode.net/#cryptoreal-dev).

### Miscellaneous
- [Assets Attribution](assets-attribution.md)
- [cryptoreal.conf Configuration File](cryptoreal-conf.md)
- [Files](files.md)
- [Fuzz-testing](fuzzing.md)
- [Reduce Memory](reduce-memory.md)
- [Reduce Traffic](reduce-traffic.md)
- [Tor Support](tor.md)
- [Init Scripts (systemd/upstart/openrc)](init.md)
- [ZMQ](zmq.md)
- [PSBT support](psbt.md)

License
---------------------
Distributed under the [MIT software license](/COPYING).
