#!/usr/bin/env bash
# Copyright (c) 2016-2019 The Cryptoreal Core developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

export LC_ALL=C
TOPDIR=${TOPDIR:-$(git rev-parse --show-toplevel)}
BUILDDIR=${BUILDDIR:-$TOPDIR}

BINDIR=${BINDIR:-$BUILDDIR/src}
MANDIR=${MANDIR:-$TOPDIR/doc/man}

CRYPTOREALD=${CRYPTOREALD:-$BINDIR/cryptoreald}
CRYPTOREALCLI=${CRYPTOREALCLI:-$BINDIR/cryptoreal-cli}
CRYPTOREALTX=${CRYPTOREALTX:-$BINDIR/cryptoreal-tx}
WALLET_TOOL=${WALLET_TOOL:-$BINDIR/cryptoreal-wallet}
CRYPTOREALQT=${CRYPTOREALQT:-$BINDIR/qt/cryptoreal-qt}

[ ! -x $CRYPTOREALD ] && echo "$CRYPTOREALD not found or not executable." && exit 1

# The autodetected version git tag can screw up manpage output a little bit
read -r -a CRLVER <<< "$($CRYPTOREALCLI --version | head -n1 | awk -F'[ -]' '{ print $6, $7 }')"

# Create a footer file with copyright content.
# This gets autodetected fine for cryptoreald if --version-string is not set,
# but has different outcomes for cryptoreal-qt and cryptoreal-cli.
echo "[COPYRIGHT]" > footer.h2m
$CRYPTOREALD --version | sed -n '1!p' >> footer.h2m

for cmd in $CRYPTOREALD $CRYPTOREALCLI $CRYPTOREALTX $WALLET_TOOL $CRYPTOREALQT; do
  cmdname="${cmd##*/}"
  help2man -N --version-string=${CRLVER[0]} --include=footer.h2m -o ${MANDIR}/${cmdname}.1 ${cmd}
  sed -i "s/\\\-${CRLVER[1]}//g" ${MANDIR}/${cmdname}.1
done

rm -f footer.h2m
