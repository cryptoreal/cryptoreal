#!/sbin/openrc-run

# backward compatibility for existing gentoo layout 
#
if [ -d "/var/lib/cryptoreal/.cryptoreal" ]; then
	CRYPTOREALD_DEFAULT_DATADIR="/var/lib/cryptoreal/.cryptoreal"
else
	CRYPTOREALD_DEFAULT_DATADIR="/var/lib/cryptoreald"
fi

CRYPTOREALD_CONFIGFILE=${CRYPTOREALD_CONFIGFILE:-/etc/cryptoreal/cryptoreal.conf}
CRYPTOREALD_PIDDIR=${CRYPTOREALD_PIDDIR:-/var/run/cryptoreald}
CRYPTOREALD_PIDFILE=${CRYPTOREALD_PIDFILE:-${CRYPTOREALD_PIDDIR}/cryptoreald.pid}
CRYPTOREALD_DATADIR=${CRYPTOREALD_DATADIR:-${CRYPTOREALD_DEFAULT_DATADIR}}
CRYPTOREALD_USER=${CRYPTOREALD_USER:-${CRYPTOREAL_USER:-cryptoreal}}
CRYPTOREALD_GROUP=${CRYPTOREALD_GROUP:-cryptoreal}
CRYPTOREALD_BIN=${CRYPTOREALD_BIN:-/usr/bin/cryptoreald}
CRYPTOREALD_NICE=${CRYPTOREALD_NICE:-${NICELEVEL:-0}}
CRYPTOREALD_OPTS="${CRYPTOREALD_OPTS:-${CRYPTOREAL_OPTS}}"

name="Cryptoreal Core Daemon"
description="Cryptoreal cryptocurrency P2P network daemon"

command="/usr/bin/cryptoreald"
command_args="-pid=\"${CRYPTOREALD_PIDFILE}\" \
		-conf=\"${CRYPTOREALD_CONFIGFILE}\" \
		-datadir=\"${CRYPTOREALD_DATADIR}\" \
		-daemon \
		${CRYPTOREALD_OPTS}"

required_files="${CRYPTOREALD_CONFIGFILE}"
start_stop_daemon_args="-u ${CRYPTOREALD_USER} \
			-N ${CRYPTOREALD_NICE} -w 2000"
pidfile="${CRYPTOREALD_PIDFILE}"

# The retry schedule to use when stopping the daemon. Could be either
# a timeout in seconds or multiple signal/timeout pairs (like
# "SIGKILL/180 SIGTERM/300")
retry="${CRYPTOREALD_SIGTERM_TIMEOUT}"

depend() {
	need localmount net
}

# verify
# 1) that the datadir exists and is writable (or create it)
# 2) that a directory for the pid exists and is writable
# 3) ownership and permissions on the config file
start_pre() {
	checkpath \
	-d \
	--mode 0750 \
	--owner "${CRYPTOREALD_USER}:${CRYPTOREALD_GROUP}" \
	"${CRYPTOREALD_DATADIR}"

	checkpath \
	-d \
	--mode 0755 \
	--owner "${CRYPTOREALD_USER}:${CRYPTOREALD_GROUP}" \
	"${CRYPTOREALD_PIDDIR}"

	checkpath -f \
	-o ${CRYPTOREALD_USER}:${CRYPTOREALD_GROUP} \
	-m 0660 \
	${CRYPTOREALD_CONFIGFILE}

	checkconfig || return 1
}

checkconfig()
{
	if ! grep -qs '^rpcpassword=' "${CRYPTOREALD_CONFIGFILE}" ; then
		eerror ""
		eerror "ERROR: You must set a secure rpcpassword to run cryptoreald."
		eerror "The setting must appear in ${CRYPTOREALD_CONFIGFILE}"
		eerror ""
		eerror "This password is security critical to securing wallets "
		eerror "and must not be the same as the rpcuser setting."
		eerror "You can generate a suitable random password using the following "
		eerror "command from the shell:"
		eerror ""
		eerror "bash -c 'tr -dc a-zA-Z0-9 < /dev/urandom | head -c32 && echo'"
		eerror ""
		eerror "It is recommended that you also set alertnotify so you are "
		eerror "notified of problems:"
		eerror ""
		eerror "ie: alertnotify=echo %%s | mail -s \"Cryptoreal Alert\"" \
			"admin@foo.com"
		eerror ""
		return 1
	fi
}
