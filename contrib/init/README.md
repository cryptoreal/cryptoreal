Sample configuration files for:
```
SystemD: cryptoreald.service
Upstart: cryptoreald.conf
OpenRC:  cryptoreald.openrc
         cryptoreald.openrcconf
CentOS:  cryptoreald.init
macOS:   org.cryptoreal.cryptoreald.plist
```
have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
