description "Cryptoreal Core Daemon"

start on runlevel [2345]
stop on starting rc RUNLEVEL=[016]

env CRYPTOREALD_BIN="/usr/bin/cryptoreald"
env CRYPTOREALD_USER="cryptoreal"
env CRYPTOREALD_GROUP="cryptoreal"
env CRYPTOREALD_PIDDIR="/var/run/cryptoreald"
# upstart can't handle variables constructed with other variables
env CRYPTOREALD_PIDFILE="/var/run/cryptoreald/cryptoreald.pid"
env CRYPTOREALD_CONFIGFILE="/etc/cryptoreal/cryptoreal.conf"
env CRYPTOREALD_DATADIR="/var/lib/cryptoreald"

expect fork

respawn
respawn limit 5 120
kill timeout 600

pre-start script
    # this will catch non-existent config files
    # cryptoreald will check and exit with this very warning, but it can do so
    # long after forking, leaving upstart to think everything started fine.
    # since this is a commonly encountered case on install, just check and
    # warn here.
    if ! grep -qs '^rpcpassword=' "$CRYPTOREALD_CONFIGFILE" ; then
        echo "ERROR: You must set a secure rpcpassword to run cryptoreald."
        echo "The setting must appear in $CRYPTOREALD_CONFIGFILE"
        echo
        echo "This password is security critical to securing wallets "
        echo "and must not be the same as the rpcuser setting."
        echo "You can generate a suitable random password using the following "
        echo "command from the shell:"
        echo
        echo "bash -c 'tr -dc a-zA-Z0-9 < /dev/urandom | head -c32 && echo'"
        echo
        echo "It is recommended that you also set alertnotify so you are "
        echo "notified of problems:"
        echo
        echo "ie: alertnotify=echo %%s | mail -s \"Cryptoreal Alert\"" \
            "admin@foo.com"
        echo
        exit 1
    fi

    mkdir -p "$CRYPTOREALD_PIDDIR"
    chmod 0755 "$CRYPTOREALD_PIDDIR"
    chown $CRYPTOREALD_USER:$CRYPTOREALD_GROUP "$CRYPTOREALD_PIDDIR"
    chown $CRYPTOREALD_USER:$CRYPTOREALD_GROUP "$CRYPTOREALD_CONFIGFILE"
    chmod 0660 "$CRYPTOREALD_CONFIGFILE"
end script

exec start-stop-daemon \
    --start \
    --pidfile "$CRYPTOREALD_PIDFILE" \
    --chuid $CRYPTOREALD_USER:$CRYPTOREALD_GROUP \
    --exec "$CRYPTOREALD_BIN" \
    -- \
    -pid="$CRYPTOREALD_PIDFILE" \
    -conf="$CRYPTOREALD_CONFIGFILE" \
    -datadir="$CRYPTOREALD_DATADIR" \
    -disablewallet \
    -daemon

