// Copyright (c) 2011-2014 The Cryptoreal Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef CRYPTOREAL_QT_CRYPTOREALADDRESSVALIDATOR_H
#define CRYPTOREAL_QT_CRYPTOREALADDRESSVALIDATOR_H

#include <QValidator>

/** Base58 entry widget validator, checks for valid characters and
 * removes some whitespace.
 */
class CryptorealAddressEntryValidator : public QValidator
{
    Q_OBJECT

public:
    explicit CryptorealAddressEntryValidator(QObject *parent);

    State validate(QString &input, int &pos) const override;
};

/** Cryptoreal address widget validator, checks for a valid cryptoreal address.
 */
class CryptorealAddressCheckValidator : public QValidator
{
    Q_OBJECT

public:
    explicit CryptorealAddressCheckValidator(QObject *parent);

    State validate(QString &input, int &pos) const override;
};

#endif // CRYPTOREAL_QT_CRYPTOREALADDRESSVALIDATOR_H
