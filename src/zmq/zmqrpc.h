// Copyright (c) 2018 The Cryptoreal Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef CRYPTOREAL_ZMQ_ZMQRPC_H
#define CRYPTOREAL_ZMQ_ZMQRPC_H

class CRPCTable;

void RegisterZMQRPCCommands(CRPCTable& t);

#endif // CRYPTOREAL_ZMQ_ZMRRPC_H
